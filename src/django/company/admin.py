from company.models import Casting, Director, Movie, Person, Producers
from django.contrib import admin

# Register your models here.

admin.site.register(Person)
admin.site.register(Casting)
admin.site.register(Director)
admin.site.register(Movie)
admin.site.register(Producers)
