from django.db import models

# Create your models here.
class Person(models.Model):
    '''
        Person model
    '''
    last_name = models.CharField(
        max_length=50,
        verbose_name=u'Last Name',
        null=False, blank=False)

    first_name = models.CharField(
        max_length=50,
        verbose_name=u'First Name',
        null=False, blank=False)

    aliases = models.CharField(
        max_length=70,
        verbose_name=u'Aliases',
        null=False, blank=False)

    def __unicode__(self):
        return '%s, %s' % (self.last_name, self.first_name)

    def __str__(self):
        return '%s, %s' % (self.last_name, self.first_name)

    class Meta:
        db_table = 'person'

    class Admin:
        pass


class Movie(models.Model):
    '''
        Movie model
    '''
    title = models.CharField(
        max_length=50,
        verbose_name=u'Title',
        null=False, blank=False)

    release_year = models.PositiveSmallIntegerField(
        verbose_name=u'Release Year',
        null=False, blank=False)

    def __unicode__(self):
        return '%s (%d)' % (self.title, self.release_year)

    def __str__(self):
        return '%s (%d)' % (self.title, self.release_year)

    class Meta:
        db_table = 'movie'

    class Admin:
        pass

class Casting(models.Model):
    '''
        Casting table
    '''
    movie = models.ForeignKey(
        Movie,
        verbose_name=u'Movie',
        db_column='id_movie',
        null=False, blank=False,
        on_delete=models.PROTECT,
        related_name='casting')

    person = models.ForeignKey(
        Person,
        verbose_name=u'Person',
        db_column='id_person',
        null=False, blank=False,
        on_delete=models.PROTECT,
        related_name='movies_as_casting')

    def __unicode__(self):
        return '%s - %s' % (self.movie, self.person)

    def __str__(self):
        return '%s - %s' % (self.movie, self.person)

    class Meta:
        db_table = 'casting'

    class Admin:
        pass


class Director(models.Model):
    '''
        Director table
    '''
    movie = models.ForeignKey(
        Movie,
        verbose_name=u'Movie',
        db_column='id_movie',
        null=False, blank=False,
        on_delete=models.PROTECT,
        related_name='directors')

    person = models.ForeignKey(
        Person,
        verbose_name=u'Person',
        db_column='id_person',
        null=False, blank=False,
        on_delete=models.PROTECT,
        related_name='movies_as_director')

    def __unicode__(self):
        return '%s - %s' % (self.movie, self.person)

    def __str__(self):
        return '%s - %s' % (self.movie, self.person)

    class Meta:
        db_table = 'director'

    class Admin:
        pass


class Producers(models.Model):
    '''
        Producers table
    '''
    movie = models.ForeignKey(
        Movie,
        verbose_name=u'Movie',
        db_column='id_movie',
        null=False, blank=False,
        on_delete=models.PROTECT,
        related_name='producers')

    person = models.ForeignKey(
        Person,
        verbose_name=u'Person',
        db_column='id_person',
        null=False, blank=False,
        on_delete=models.PROTECT,
        related_name='movies_as_producer')

    def __unicode__(self):
        return '%s - %s' % (self.movie, self.person)

    def __str__(self):
        return '%s - %s' % (self.movie, self.person)

    class Meta:
        db_table = 'producer'

    class Admin:
        pass
