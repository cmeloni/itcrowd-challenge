from rest_framework import serializers
from company.models import Casting, Director, Movie, Person, Producers
from roman import toRoman

class MovAsDirectorSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Director table
    '''
    id = serializers.IntegerField(source='movie.id')
    title = serializers.CharField(source='movie.title')
    class Meta:
        model = Director
        fields = ['id', 'title']

class MovAsCastingSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Casting table
    '''
    id = serializers.IntegerField(source='movie.id')
    title = serializers.CharField(source='movie.title')
    class Meta:
        model = Casting
        fields = ['id', 'title']

class MovAsProducersSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Producers table
    '''
    id = serializers.IntegerField(source='movie.id')
    title = serializers.CharField(source='movie.title')
    class Meta:
        model = Producers
        fields = ['id', 'title']


class PersonSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Person table
    '''
    movies_as_casting = MovAsCastingSerializer(many=True, read_only=True)
    movies_as_director = MovAsDirectorSerializer(many=True, read_only=True)
    movies_as_producer = MovAsProducersSerializer(many=True, read_only=True)

    class Meta:
        model = Person
        fields = '__all__'

class MovCastingSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Casting table
    '''
    id = serializers.IntegerField(source='person.id')
    first_name = serializers.CharField(source='person.first_name')
    last_name = serializers.CharField(source='person.last_name')
    class Meta:
        model = Casting
        fields = ['id', 'first_name', 'last_name']

class MovDirectorSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Directors table
    '''
    id = serializers.IntegerField(source='person.id')
    first_name = serializers.CharField(source='person.first_name')
    last_name = serializers.CharField(source='person.last_name')
    class Meta:
        model = Director
        fields = ['id', 'first_name', 'last_name']

class MovProducersSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Producers table
    '''
    id = serializers.IntegerField(source='person.id')
    first_name = serializers.CharField(source='person.first_name')
    last_name = serializers.CharField(source='person.last_name')
    class Meta:
        model = Producers
        fields = ['id', 'first_name', 'last_name']

class MovieSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Movie table
    '''
    casting = MovCastingSerializer(many=True, read_only=True)
    directors = MovDirectorSerializer(many=True, read_only=True)
    producers = MovProducersSerializer(many=True, read_only=True)

    roman_year = serializers.SerializerMethodField('get_roman_year')

    def get_roman_year(self, obj):
        return toRoman(obj.release_year)

    class Meta:
        model = Movie
        fields = '__all__'

class CastingSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Casting table
    '''
    class Meta:
        model = Casting
        fields = '__all__'

class DirectorSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Director table
    '''
    class Meta:
        model = Director
        fields = '__all__'


class ProducersSerializer(serializers.ModelSerializer):
    '''
        Serializer for the Company.Producers table
    '''
    class Meta:
        model = Producers
        fields = '__all__'
