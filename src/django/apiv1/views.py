from apiv1.serializers import PersonSerializer, MovieSerializer, CastingSerializer, DirectorSerializer, ProducersSerializer
from company.models import Casting, Director, Movie, Person, Producers
from django.shortcuts import render
from rest_framework import generics, viewsets

# Create your views here.
class PersonList(generics.ListCreateAPIView,
                    generics.RetrieveUpdateDestroyAPIView,
                    viewsets.GenericViewSet):
    '''
    
    '''
    queryset = Person.objects.all()
    serializer_class = PersonSerializer

class MovieList(generics.ListCreateAPIView,
                    generics.RetrieveUpdateDestroyAPIView,
                    viewsets.GenericViewSet):
    '''
    
    '''
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

class CastingList(generics.ListCreateAPIView,
                    generics.RetrieveUpdateDestroyAPIView,
                    viewsets.GenericViewSet):
    '''

    '''
    queryset = Casting.objects.all()
    serializer_class = CastingSerializer

class DirectorList(generics.ListCreateAPIView,
                    generics.RetrieveUpdateDestroyAPIView,
                    viewsets.GenericViewSet):
    '''

    '''
    queryset = Director.objects.all()
    serializer_class = DirectorSerializer

class ProducersList(generics.ListCreateAPIView,
                    generics.RetrieveUpdateDestroyAPIView,
                    viewsets.GenericViewSet):
    '''

    '''
    queryset = Producers.objects.all()
    serializer_class = ProducersSerializer
