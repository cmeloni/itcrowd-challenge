from django.conf.urls import include, url
from rest_framework import routers
from apiv1.views import PersonList, MovieList, CastingList, DirectorList, ProducersList
from rest_framework.authtoken import views

router = routers.DefaultRouter()

router.register(r'person', PersonList)
router.register(r'movie', MovieList)
router.register(r'casting', CastingList)
router.register(r'director', DirectorList)
router.register(r'producer', ProducersList)

urlpatterns = [
    url(r'', include(router.urls)),
    url(r'^token-auth/', views.obtain_auth_token, name='token-auth')
]
