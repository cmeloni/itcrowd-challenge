from django.urls import reverse, resolve
#from django.test import TestCase
from rest_framework.test import APITestCase, APIClient
from requests.auth import HTTPBasicAuth
from mixer.backend.django import mixer
from django.contrib.auth.models import User, AnonymousUser
from company.models import Person, Movie, Casting
import json

import pytest

@pytest.mark.django_db
class TestMovieAnon(APITestCase):
    '''
    This test create a movie and get it from api without authentication
    check that roman number is correctly formed
    '''
    @classmethod
    def setUp(self):
        '''
            Setup a Movie in a database
        '''
        self.mdata = {
            'title':'Toy Story',
            'release_year': 1974
        }
        self.movie = Movie.objects.create(
            **self.mdata
        )
        self.movie.save()

    def test_anonymous_movie_get(self):
        '''
            Test api
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('movie-detail', kwargs={'pk': self.movie.id})

        response = self.client.get(path)
        
        assert response.status_code == 200
        assert response.data['title'] == self.mdata['title']
        assert response.data['release_year'] == self.mdata['release_year']
        assert response.data['roman_year'] == 'MCMLXXIV'

@pytest.mark.django_db
class TestPersonAnon(APITestCase):
    '''
    This test create a Person and get it from api without authentication
    '''
    @classmethod
    def setUp(self):
        '''
            Setup a person in a database
        '''
        self.pdata = {
            'last_name':'Doe',
            'first_name':'John',
            'aliases':'JDoe'
        }
        self.person = Person.objects.create(
            **self.pdata
        )
        self.person.save()

    def test_anonymous_person_get(self):
        '''
            Test de la API para loguearse anonimamente
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('person-list')

        response = self.client.get(path)
        
        assert response.status_code == 200
        assert response.data[0]['first_name'] == self.pdata['first_name']
        assert response.data[0]['last_name'] == self.pdata['last_name']
        assert response.data[0]['aliases'] == self.pdata['aliases']

@pytest.mark.django_db
class TestPersonMovieAnon(APITestCase):
    '''
    This test create a Person and Movie, related, and get it from api without authentication
    '''
    @classmethod
    def setUp(self):
        '''
            Setup a person in a database
        '''
        self.pdata = {
            'last_name':'Doe',
            'first_name':'John',
            'aliases':'JDoe'
        }
        self.person = Person.objects.create(
            **self.pdata
        )
        self.person.save()

        '''
            Setup a Movie in a database
        '''
        self.mdata = {
            'title':'Toy Story',
            'release_year': 1974
        }
        self.movie = Movie.objects.create(
            **self.mdata
        )
        self.movie.save()

        self.cdata = {
            'person': self.person,
            'movie': self.movie
        }
        self.casting = Casting.objects.create(
            **self.cdata
        )
        self.casting.save()

    def test_anonymous_person_get_casting(self):
        '''
            Test de la API para loguearse anonimamente
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('person-detail', kwargs={'pk': self.person.id})

        response = self.client.get(path)
        
        assert response.status_code == 200
        assert response.data['first_name'] == self.pdata['first_name']
        assert response.data['last_name'] == self.pdata['last_name']
        assert response.data['aliases'] == self.pdata['aliases']
        assert response.data['movies_as_casting'][0]['title'] == self.casting.movie.title

    def test_anonymous_movie_get_casting(self):
        '''
            Test de la API para loguearse anonimamente
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('movie-detail', kwargs={'pk': self.movie.id})

        response = self.client.get(path)
        
        assert response.status_code == 200
        assert response.data['title'] == self.mdata['title']
        assert response.data['release_year'] == self.mdata['release_year']
        assert response.data['roman_year'] == 'MCMLXXIV'
        assert response.data['casting'][0]['first_name'] == self.casting.person.first_name


@pytest.mark.django_db
class TestApiPersonAuth(APITestCase):
    '''
    
    '''
    @classmethod
    def setUp(self):
        '''
            
        '''
        self.user = User.objects.create_user(
            username='jacob', email='jacob@…', password='top_secret')

        path = reverse('token-auth')

        client = APIClient()
        
        response = client.post(path, {
                'username': 'jacob',
                'password': 'top_secret'
                }, format='json')

        print(response.data)

        self.token = response.data['token']

    def test_create_person_without_token(self):
        '''
             by Token
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('person-list')

        self.pdata = {
                'first_name': 'Martiniano',
                'last_name': 'Meloni',
                'aliases': 'Cabeza'
            }

        response = self.client.post(path, self.pdata, format='json')

        print(repr(response.data))
        assert response.status_code == 403

    def test_create_person_with_token(self):
        '''
             by Token
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('person-list')

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)

        self.pdata = {
                'first_name': 'Martiniano',
                'last_name': 'Meloni',
                'aliases': 'Cabeza'
            }

        response = self.client.post(path, self.pdata, format='json')

        print(repr(response.data))
        assert response.status_code == 201

    def test_create_movie_with_token(self):
        '''
             by Token
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('movie-list')

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)

        '''
            Setup a Movie in a database via authenticated API
        '''
        self.mdata = {
            'title':'Toy Story',
            'release_year': 1974
        }

        response = self.client.post(path, self.mdata, format='json')

        print(repr(response.data))
        assert response.status_code == 201
        assert response.data['title'] == self.mdata['title']
        assert response.data['release_year'] == self.mdata['release_year']
        assert response.data['roman_year'] == 'MCMLXXIV'



@pytest.mark.django_db
class TestApiPersonAuth(APITestCase):
    '''
    
    '''
    @classmethod
    def setUp(self):
        '''
            
        '''
        self.user = User.objects.create_user(
            username='jacob', email='jacob@…', password='top_secret')

        path = reverse('token-auth')

        client = APIClient()
        
        response = client.post(path, {
                'username': 'jacob',
                'password': 'top_secret'
                }, format='json')

        print(response.data)

        self.token = response.data['token']

        '''
            Setup a person in a database
        '''
        self.pdata = {
            'last_name':'Doe',
            'first_name':'John',
            'aliases':'JDoe'
        }
        self.person = Person.objects.create(
            **self.pdata
        )
        self.person.save()

        '''
            Setup a Movie in a database
        '''
        self.mdata = {
            'title':'Toy Story',
            'release_year': 1974
        }
        self.movie = Movie.objects.create(
            **self.mdata
        )
        self.movie.save()

    def test_casting_api_without_token(self):
        '''
             by Token
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('casting-list')

        '''
            Setup a Casting in a database via authenticated API
        '''
        self.cdata = {
            'movie': self.movie.id,
            'person': self.person.id
        }

        response = self.client.post(path, self.cdata, format='json')

        print(repr(response.data))
        assert response.status_code == 403


    def test_casting_api_with_token(self):
        '''
             by Token
        '''
        # Armo la URL con parametros con ayuda de reverse
        path = reverse('casting-list')

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)

        '''
            Setup a Casting in a database via authenticated API
        '''
        self.cdata = {
            'movie': self.movie.id,
            'person': self.person.id
        }

        response = self.client.post(path, self.cdata, format='json')

        print(repr(response.data))
        assert response.status_code == 201
        assert response.data['movie'] == self.movie.id
        assert response.data['person'] == self.person.id

