import os
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url

urlpatterns = [
    url(r'^%s/' % os.environ.get('DJANGO_ADMIN_PATH'), admin.site.urls),
    url(r'^%s/doc/' % os.environ.get('DJANGO_ADMIN_PATH'),  include('django.contrib.admindocs.urls')),
    url(r'^apiv1/', include('apiv1.urls')),
]