#!/bin/bash

cd /usr/src/app
if [ ! -f manage.py ]
then
	django-admin startproject app .
fi

python manage.py makemigrations
python manage.py migrate --noinput

if [[ ! -z "${DJANGO_SU_ADMIN}" ]]; then
	echo "from django.contrib.auth import get_user_model; get_user_model().objects.filter(username='$DJANGO_SU_ADMIN').exists() or  get_user_model().objects.create_superuser('$DJANGO_SU_ADMIN', '$DJANGO_SU_EMAIL', '$DJANGO_SU_PASSWORD')" | python manage.py shell
fi

python manage.py runserver 0.0.0.0:8000
