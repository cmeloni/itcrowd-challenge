# ITCrowd Challenge

Implementation of an API for the data model of a company that provides information about movies.

# Features

  - A REST API is provided to access movies and models of people.
  - Safe methods are publicly available, no authentication is required.
  - Unsafe methods are only available to authenticated users.
  - Film documents include complete documents to people in their different roles.
  - The Person's documents include complete film documents in the different roles the Person has.
  - The documents for the film include the Release Year in Roman numerals. This field is calculated on the fly.

# Tech

> To implement this solution, i tried to use all the features provided by the DRF framework: Authentication, Serializers, Routes, Viewsets, Generic Views, Testing.

- The development environment is based on docker. Docker-compose.yml is used to define the project manifest.
- All required libraries are documented in the config/django-image/requirements.txt file.
- The project contains an example environment variables file (.env_demo) where you can customize some parameters.
- The example code is using the [**httpie**](https://httpie.org/) client.

# Authentication

The authentication capacity with the default user of the project is provided. This user is created at the first start, along with the structure of the database.
This username and password are previously defined with the environment variables file.

The authentication methods provided are:
- Session Authentication
- Basic Authentication
- Token Authentication

# Installation

> docker and docker-compose are required in the system


```sh
$ git clone https://gitlab.com/cmeloni/itcrowd-challenge.git
$ cd itcrowd-challenge
$ cp .env_demo .env
$ docker-compose up
```

# Testing
>You should never develop an API without its test. Cesar Meloni
I said never!!!! ;)

A series of tests on APIs is provided. A series of tests on APIs is provided. To test the API run the command:
```sh
$ docker-compose exec django py.test
```

# Endpoints

>Each endpoint has a base URL (**/apiv1/base**) and a URL to access objects (**/apiv1/base/<object_id>/**).


| Model | Base Endpoint | Object Endpoint | Methods Allow (* Anonymous) |
| ------ | ------ | ------ | ------ |
| person | /apiv1/person/ | /apiv1/person/<object_id>/ | *GET, POST, PUT, PATCH, DELETE, *HEAD, *OPTIONS |
| movie | /apiv1/movie/ | /apiv1/movie/<object_id>/ | *GET, POST, PUT, PATCH, DELETE, *HEAD, *OPTIONS |
| casting | /apiv1/casting/ | /apiv1/casting/<object_id>/ | *GET, POST, PUT, PATCH, DELETE, *HEAD, *OPTIONS |
| director | /apiv1/director/ | /apiv1/director/<object_id>/ | *GET, POST, PUT, PATCH, DELETE, *HEAD, *OPTIONS |
| producer | /apiv1/producer/ | /apiv1/producer/<object_id>/ | *GET, POST, PUT, PATCH, DELETE, *HEAD, *OPTIONS |


>The example code is using the [**httpie**](https://httpie.org/) client.

# Person

This endpoint provides access and manipulation of Person objects.

 - Add person (Authenticated):
 ```sh
    $ http -a admin:1234 POST http://127.0.0.1:8000/apiv1/person/ first_name=John last_name=Doe aliases=JDoe
 ```
 - Response:
 ```sh
HTTP/1.1 201 Created
Allow: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS
Content-Length: 135
Content-Type: application/json
Date: Sat, 03 Jul 2020 16:45:28 GMT
Server: WSGIServer/0.2 CPython/3.7.4
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "aliases": "JDoe",
    "first_name": "John",
    "id": 1,
    "last_name": "Doe",
    "movies_as_casting": [],
    "movies_as_director": [],
    "movies_as_producer": []
}
 ```
 - List Persons:
 
```sh
    $ http http://127.0.0.1:8000/apiv1/person/
```
 - Response:
```sh
HTTP/1.1 200 OK
Allow: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS
Content-Length: 1188
Content-Type: application/json
Date: Sat, 04 Jul 2020 16:48:52 GMT
Server: WSGIServer/0.2 CPython/3.7.4
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

[
    {
        "aliases": "JDoe",
        "first_name": "John",
        "id": 1,
        "last_name": "Doe",
        "movies_as_casting": [],
        "movies_as_director": [],
        "movies_as_producer": []
    }
]
```

# Movie

This endpoint provides access and manipulation of Movie objects.

 - Add movie (Authenticated):
 ```sh
    $ http -a admin:1234 POST http://127.0.0.1:8000/apiv1/movie/ title="Toy Story" release_year=2000
 ```
 - Response:
 
```sh
HTTP/1.1 201 Created
Allow: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS
Content-Length: 109
Content-Type: application/json
Date: Sat, 03 Jul 2020 16:54:45 GMT
Server: WSGIServer/0.2 CPython/3.7.4
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "casting": [],
    "directors": [],
    "id": 3,
    "producers": [],
    "release_year": 2000,
    "roman_year": "MM",
    "title": "Toy Story"
}
```

 - List Movies:


```sh
    $ http http://127.0.0.1:8000/apiv1/movie/
```
 - Response:

```sh
HTTP/1.1 200 OK
Allow: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS
Content-Length: 713
Content-Type: application/json
Date: Sat, 04 Jul 2020 16:56:28 GMT
Server: WSGIServer/0.2 CPython/3.7.4
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

[
    {
        "casting": [],
        "directors": [],
        "id": 3,
        "producers": [],
        "release_year": 2000,
        "roman_year": "MM",
        "title": "Toy Story"
    }
]
```

# Casting, Producers and Directors

This Endpoint allows manipulate the Casting, Producers and Directors of a movie. 

 - Add Casting to Movie: (See other Endpoints urls)

```sh
$ http -a admin:1234 POST http://127.0.0.1:8000/apiv1/casting/ movie=3 person=1
```

 - Response:

```sh
HTTP/1.1 201 Created
Allow: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS
Content-Length: 29
Content-Type: application/json
Date: Sat, 03 Jul 2020 17:04:08 GMT
Server: WSGIServer/0.2 CPython/3.7.4
Vary: Accept, Cookie
X-Content-Type-Options: nosniff
X-Frame-Options: DENY

{
    "id": 1,
    "movie": 3,
    "person": 1
}
```

**Cesar Meloni**
